import pymysql

try:
    connection = pymysql.connect(
        host='mysql-rfam-public.ebi.ac.uk',
        port=4497,
        user='rfamro',
        password='',
        database='Rfam',
        cursorclass=pymysql.cursors.DictCursor
    )
    print("#" * 20)

    try:
        with connection.cursor() as cursor:
            select_all_rows = "SELECT * FROM `full_region` LIMIT 1"
            cursor.execute(select_all_rows)

            rows = cursor.fetchall()
            for row in rows:
                print(row)
            print("#" * 20)

            select_all_rows = "SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end" \
                              " FROM full_region fr, rfamseq rf, taxonomy tx" \
                              " WHERE rf.ncbi_id = tx.ncbi_id" \
                              " AND fr.rfamseq_acc = rf.rfamseq_acc" \
                              " AND tx.ncbi_id = 10116" \
                              " AND is_significant = 1"
            cursor.execute(select_all_rows)

            rows = cursor.fetchall()
            for row in rows:
                print(row)
            print("#" * 20)


    except Exception as ex:
        print(ex)
    finally:
        connection.close()

except Exception as ex:
    print("Connection refused...")
    print(ex)

#//select * from fr.fram_acc limit 1;
